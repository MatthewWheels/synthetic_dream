#include "ofApp.h"

//SEE APP.H FIRST

//--------------------------------------------------------------

void ofApp::setup(){
    
    text.set(255, 110, 199);
    //GUI SETUP
 
    
    //WEBCAM AND IMAGE SETUP
    webcam.setup(1280,720);
    image.allocate(1280,720, OF_IMAGE_COLOR);
    
    
    //Sets cam at slightly left of center
  cam.setPosition(345, 295, 1000);
  
    //Adds the GUI panel for amplitude
   ofSetVerticalSync(true);
    gui.setup();
    solidity.setup();
    gui.add(Amplitude.setup("Amplitude", 1, 0, 5));
      gui.setPosition(10, 20);
    gui.setTextColor(text);
    
    //Adds the GUI Panel to switch between WireFrame and Solids
    
    solidity.add(wires.setup("Wire Frame vs Solid", true));
    solidity.setPosition(10,60);
   wires=true;
    
    
//Grid SETUP
    for(int i=1; i<nGrids; i++){
             grid[i].setup(i);
         }
  
    }


//--------------------------------------------------------------
void ofApp::update(){
    
    amp=Amplitude+addition;
   //checks if the webcam is running
    if(webcam.isInitialized()){
           webcam.update();
           if(webcam.isFrameNew()){
               
               
            //creates an image from the webcam which is passed to the Gridd class update section
               image.setFromPixels(webcam.getPixels());
               
   //i controls the y axis
               //image is the webcame info
               //Amplitude controls the z axis
               //Wires control whether a wireframe is displayed or its a solid mesh
    for(int i=1; i<nGrids; i++){
             grid[i].update(i, image, amp);
         }
}
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    //Sets up draw function
    ofBackground(0);
    ofEnableDepthTest();
    cam.begin();
    

    for(int i=1; i<nGrids; i++){
    grid[i].draw(wires);
    }
    
    //ends draw functions
    cam.end();
   
    cam.draw();

 
  gui.draw();
  solidity.draw();
    //cam.end();
    ofDisableDepthTest();
    
}

//--------------------------------------------------------------

void ofApp::keyPressed(int key){
    if (key == 'w'){
        addition+= 0.1;
    }
    if (key == 's'){
        addition-=0.1;
    }
    if (key == 'a'){
        wires=false;
    }
    if (key == 'd'){
           wires=true;
       }
}
