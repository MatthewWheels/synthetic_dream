#pragma once

#include "ofMain.h"
#include "Grid.hpp"
#include "ofxGui.h"
#define nGrids 30

class ofApp : public ofBaseApp{
    
    //The General concept of this is to create a triangle strip mesh class which creates a single horizontal strip across. The class is then put into an array which generates several instances stacked on top of each other. The vertices of these meshes are to be affected by the brightness of a corresponding pixel from the webcam on the z axes. The idea is to make it look like a 3D wireframe of objects in the webcam.

    //Elements yet to be added: GUI control for the amplification of the z axis, GUI control to change the color of the tallest Vertexes
    
	public:
		void setup();
		void update();
		void draw();
    void keyPressed(int key);
    

 
    Gridclass grid[30];
    //camera
    ofCamera cam;
    //The webcam and the image made by it
    ofVideoGrabber webcam;
    ofImage image;
    //controls z value amplification
    ofxFloatSlider Amplitude;
    float amp, addition;
    //controls wireframe vs solid
    ofxToggle wires;
    ofColor text;
    
    ofxPanel gui, solidity;
  
};
