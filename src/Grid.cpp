//
//  Grid.cpp
//  Cube_Arrah
//
//  Created by Matthew Wheeler on 1/26/20.
//

#include "Grid.hpp"

void Gridclass::setup(int _y){
    
    
   //index is set to -1 so that the first vertex indexed is 0
       index=-1;
    
    
       glLineWidth(4);
    
    //This creates an array of points to create the triangle strip.
       for(int i=0; i<40; i++){
           
           grid.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           //This set of commands creates 2 vertices starting with one below and then one above before moving along the x axis and repeating.
           //After each individual vertex is added the Index is increased and an index is added for that Vertex.
           
           grid.addVertex(glm::vec3(i*20, _y*20, 0));
           grid.addColor(teal);
           index++;
           grid.addIndex(index);
         grid.addVertex(glm::vec3(i*20, (_y+1)*20, 0));
           index++;
               grid.addColor(teal);
           grid.addIndex(index);
       }
    
}
void Gridclass::update(int _x, ofImage camFrame, float _amp){
    
            //sets the U(pdate)Index to 0
          
             UIndex = 0;
            for(int j =0; j < 40; j++){
                
                
                //Generates the color pixel based on the corresponding color from the webcam.
                    ofColor pixel = camFrame.getColor( ((((j* 20)-640)*-1)+640), (((_x*20)-360)*-1)+360);
          
                //sets the teal color
                teal.set(0, pixel.getBrightness(), pixel.getBrightness(), pixel.getBrightness());
               

                
                //Sets the vertex based upon the U(pdate)Index value and the brightness of the pixel color previously generated
                    grid.setVertex(UIndex, glm::vec3(j * 20, _x*20, pixel.getBrightness()));
                
                
                //sets the color to teal

                  grid.setColor(UIndex,teal);


            
                
                UIndex++;
                
                //this gets the pixel color for the next vertex in the index
                pixel = camFrame.getColor( ((((j* 20)-640)*-1)+640), ((((_x+1)*20)-360)*-1)+360);
           
                //sets the vertex and the color of the next vertex in the index
                 grid.setVertex((UIndex), glm::vec3(j * 20, (_x+1)*20, (pixel.getBrightness()*_amp)));
                 grid.setColor(UIndex,teal);

                
                UIndex++;
                      
                

         
    }
      
}
void Gridclass::draw(bool _meshState){
    
    
 
    grid.drawWireframe();
    grid.drawVertices();
    if(_meshState==false){
    grid.draw();
    }
    
}
