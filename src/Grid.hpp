//
//  Grid.hpp
//  Cube_Arrah
//
//  Created by Matthew Wheeler on 1/26/20.
//

#ifndef Grid_hpp
#define Grid_hpp

#include <stdio.h>
#include <ofMain.h>



class Gridclass{
public:
    
    void setup(int _y);
    void update(int _x, ofImage camFrame, float _amp);
    void draw(bool _meshState);
    
    ofMesh grid;
    
    //index is the counter I use to set the index of each Vertex and UIndex is the index counter I use in the UPDATE section of the code.
    int index, UIndex;
    float height;
    ofColor teal;


};


#endif /* Grid_hpp */
