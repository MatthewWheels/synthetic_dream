#  Synthetic Dreams

By Matthew Wheeler

[Synthetic Dream](https://MatthewWheels@bitbucket.org/MatthewWheels/synthetic_dream.git)
![](https://bitbucket.org/MatthewWheels/synthetic_dream/raw/6327b1361c42d19e45e3a999172cb139b932a5bd/Synthetic_Dream_ScreenShot.png)

This program creates an array of triangle mesh classes which adjust their z-position and brightness based upon the brightness of a corresponding pixel from the webcam.

Current Temporary Controls (in lieu of functioning GUI panels):

- W increases the amplitude of the Vertices
- S decreases the amplitude of the Vertices
- A sets the mesh to be solid
- D sets the mesh to be a wire frame.

## Features that still need to be added/fixed:

- GUI slider controlling the amplitude of the vertices
- GUI toggle controlling the wireframe vs filled draw



